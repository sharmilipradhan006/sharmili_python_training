class A():

    def func(self):
        print('i am a')


class B():

    def func(self):
        print('i am B')


class C(B, A):

    def iamC(self):
        print('i am C')


c = C()
c.func()