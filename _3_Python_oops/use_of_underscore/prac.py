# a, _, c = (1, 2, 3) # a = 1, c = 3
# print(a, c)
# print(_)

a, *_, b = [7, 6, 5, 4, 3, 2, 1]
print(a, b)
print(type(_))