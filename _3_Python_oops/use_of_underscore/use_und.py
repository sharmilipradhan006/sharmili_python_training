from use_of_underscore._2_example import *
func()

"""".................................."""

import use_of_underscore._2_example as ue
print(ue._private_func())

"""
single_postunderscore...
Sometimes if you want to use Python Keywords as a variable, 
function or class names, you can use this convention for that.

You can avoid conflicts with the Python 
Keywords by adding an underscore at the end of the 
name which you want to use
"""