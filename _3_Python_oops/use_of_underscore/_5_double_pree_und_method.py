class SimpleClass:

    def __datacamp(self):
        return "datacamp"

    def call_datacamp(self):
        return self.__datacamp()


obj = SimpleClass()
print(obj.call_datacamp())  ## same as above it returns the Dobule pre underscore method
print(obj.__datacamp())
