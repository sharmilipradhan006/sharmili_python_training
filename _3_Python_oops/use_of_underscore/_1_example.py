## lopping ten times using _
for _ in range(5):
    print(_)


a, _, c = (1, 2, 3) # a = 1, c = 3
print(a, c)

## ignoring multiple values
## *(variable) used to assign multiple
# value to a variable as list while unpacking
## it's called "Extended Unpacking", only available in Python 3.x
a, *_, b = (7, 6, 5, 4, 3, 2, 1)
print(a, b)


## iterating over a list using _
## you can use _ same as a variable
languages = ["Python", "JS", "PHP", "Java"]
for _ in languages:
    print(_)

_ = 5
while _ < 10:
    print(_, end = ' ') # default value of 'end' id '\n' in python. we're changing it to space
    _ += 1


## different number systems
## you can also check whether they are correct or not by coverting them into integer using "int" method
million = 1_000_00
binary = 0b_0010
octa = 0o_64
hexa = 0x_23_ab
print('\n')
print(million)
print(binary)
print(octa)
print(hexa)