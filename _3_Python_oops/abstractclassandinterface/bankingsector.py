""" Contains payment processors for executing payments """

from abc import ABCMeta, abstractmethod


class Banking(metaclass=ABCMeta):
    """ Payment processor that does nothing, just logs """

    def __init__(self):
        self._logger = logging.getLogger(__name__)
        self._logger.setLevel(logging.INFO)

    @abstractmethod
    def process_payment(self, destination_address, amount):
        """ Execute a payment to one receiving single address

        return the transaction id or None """
        pass


class BankingBank1(Banking):
    """docstring for BankingBank1"""

    def __init__(self, arg):
        super(BankingBank1, self).__init__()
        self.arg = arg

    def process_payment(self, destination_address, amount):
        """ Execute a payment to one receiving single address

        return the transaction id or None """
        print('process payment for bank 1')


class BankingBank2(Banking):
    """docstring for BankingBank1"""

    def __init__(self, arg):
        super(BankingBank2, self).__init__()
        self.arg = arg


try:
    bb2 = BankingBank2(__name__)
    bb2.process_payment('destination_address', 100)

    bb1 = BankingBank1(__name__)
    bb1.process_payment('destination_address', 100)
except TypeError as e:
    print('Cannot process the payment.')