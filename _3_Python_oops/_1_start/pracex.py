from astroid.objects import Super


class Employee():

    def __init__(self, id, name, sal):
        self.id = id
        self.name = name
        self.sal = sal


class Dev(Employee):

    def __init__(self, id, name, sal):
        Employee.__init__(self, id, name, sal)


class Salse(Employee):

    def __init__(self, id, name, sal, inct, tar):
        Employee.__init__(self, id, name, sal)
        self.inct = inct
        self.tar = tar



def main():
    d = Dev('1', 'john', '20 lac')
    print(d.id)
    print(d.name)
    print(d.sal)

    s = Salse('2', 'mark','20 lac', '20k', '200 class a day')
    print(s.id)
    print(s.name)
    print(s.sal)
    print(s.inct)
    print(s.tar)

    l = []
    l.append(d)
    l.append(s)

    for i in l:
        if(isinstance(i, Dev)):
            print('Dev')
        elif(isinstance(i, Salse)):
            print('Salse')



if __name__ == '__main__':
    main()