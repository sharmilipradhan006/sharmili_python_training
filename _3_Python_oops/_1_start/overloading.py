# #!/usr/bin/env python
#
# class Human:
#     def _sayHello(self, name=None):
#         if name is not None:
#             print('Hello ' + name)
#         else:
#             print('Hello ')
#
#
#
#
# # Create instance
# obj = Human()
# # Call the method
# obj._sayHello()
#
#
# # Call the method with a parameter
# obj._sayHello('john')
# _sayHello

class Cal():

    def add(self, a=None, b=None):
        if(a != None and b != None):
            return a + b
        else:
            print('no result')

c = Cal()
c.add(10)
print(c.add(10, 20))
c.add()