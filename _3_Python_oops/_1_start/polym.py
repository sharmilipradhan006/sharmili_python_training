class CAT:


    def sound(self):
        print('cat sound is meaaaoooo')

class DOG:

    def sound(self):
        print('dog sound is barkkkkk')


class HORSE:

    def sound(self):
        print('HORSE sound is HHHHHHHH')


def testInterface(animal):
    animal.sound()


c = CAT()
testInterface(c)

d = DOG()
testInterface(d)

h = HORSE()
testInterface()
