import sqlite3

conn = sqlite3.connect('emp_db.db')

id  = input('Enter id:')
name  = input('Enter name:')
age  = input('Enter age:')
address  = input('Enter address:')
sal  = input('Enter sal:')

que = "INSERT INTO Employee (ID,NAME,AGE,ADDRESS,SALARY) " \
      "VALUES (:id, :name, :age, :address, :sal)"

emp_dict = {
    'id':id,
    'name':name,
    'age':age,
    'address':address,
    'sal':sal
}

conn.execute(que, emp_dict)
conn.commit()
conn.close()