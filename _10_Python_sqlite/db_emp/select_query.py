import sqlite3

conn = sqlite3.connect('emp_db.db')
print ("Opened database successfully")

cursor = conn.execute("SELECT * from Employee")

for row in cursor:
        print('==========================')
        print("ID = ", row[0])
        print("NAME = ", row[1])
        print("AGE = ", row[2])
        print("ADDRESS = ", row[3])
        print("SALARY = ", row[4], "\n")

print ("Operation done successfully")
conn.close()