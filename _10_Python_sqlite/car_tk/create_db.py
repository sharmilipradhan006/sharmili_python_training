import sqlite3

conn = sqlite3.connect('car.db')
conn.execute('''CREATE TABLE Car (ID INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
NAME TEXT NOT NULL,
TYPE TEXT NOT NULL,
COST INT NOT NULL);''')

conn.commit()
conn.close()
